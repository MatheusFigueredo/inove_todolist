import 'package:flutter/foundation.dart';

import 'localTasksUtilities.dart';

class Controller extends ChangeNotifier {
  static final Controller _singleton = Controller._internal();

  factory Controller() {
    return _singleton;
  }

  Controller._internal();

  List todoList = [];
  LocalTasksUtilities localTasksUtilities = LocalTasksUtilities();

  addTask(Map<String, dynamic> task) {
    todoList.add(task);
    saveTasks();
    notifyListeners();
  }

  remove(Map<String, dynamic> task) {
    todoList.remove(task);
    saveTasks();
    notifyListeners();
  }

  saveTasks() {
    localTasksUtilities.saveData(todoList);
  }
}
