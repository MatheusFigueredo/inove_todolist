import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:i9_todo_list/add_task_screen.dart';
import 'package:provider/provider.dart';
import 'controller.dart';
import 'todo_item.dart';

class ToDoListScreen extends StatefulWidget {
  @override
  _ToDoListScreenState createState() => _ToDoListScreenState();
}

class _ToDoListScreenState extends State<ToDoListScreen> {
  Controller controller = Controller();

  @override
  void initState() {
    super.initState();
    controller.localTasksUtilities.readData().then(
      (localTodoList) {
        setState(() {
          controller.todoList = json.decode(localTodoList);
        });
      },
    );
  }

  Future<Null> _refresh() async {
    await Future.delayed(Duration(seconds: 1));

    setState(() {
      controller.todoList.sort((a, b) {
        if (a['done'] && !b['done'])
          return 1;
        else if (!a['done'] && b['done'])
          return -1;
        else
          return 0;
      });

      controller.saveTasks();
    });
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Tarefas'),
        centerTitle: true,
        actions: [
          IconButton(
            icon: Icon(Icons.add),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (_) => AddTaskScreen(),
                ),
              );
            },
          ),
        ],
      ),
      body: Consumer<Controller>(
        builder: (context, controller, body) {
          return controller.todoList.isEmpty
              ? Center(
                  child: Text(
                    'Lista Vazia!',
                    style: TextStyle(
                      fontSize: 25,
                    ),
                  ),
                )
              : RefreshIndicator(
                  onRefresh: _refresh,
                  child: ListView.builder(
                      padding: EdgeInsets.only(top: 10.0),
                      itemCount: controller.todoList.length,
                      itemBuilder: (context, index) {
                        return ToDoItem(task: controller.todoList[index]);
                      }),
                );
        },
      ),
    );
  }
}
