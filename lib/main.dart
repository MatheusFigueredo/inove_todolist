import 'package:flutter/material.dart';
import 'package:i9_todo_list/todo_list_screen.dart';
import 'package:provider/provider.dart';

import 'controller.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<Controller>(
      create: (_) => Controller(),
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        debugShowCheckedModeBanner: false,
        home: ToDoListScreen(),
      ),
    );
  }
}
