import 'dart:io';
import 'dart:convert';
import 'package:path_provider/path_provider.dart';

class LocalTasksUtilities {
  Future<File> getFile() async {
    final directory = await getApplicationDocumentsDirectory();
    return File("${directory.path}/tasks.json");
  }

  Future<File> saveData(List todoList) async {
    String data = json.encode(todoList);

    final file = await getFile();
    return file.writeAsString(data);
  }

  Future<String> readData() async {
    try {
      final file = await getFile();

      return file.readAsString();
    } catch (e) {
      return '';
    }
  }
}
