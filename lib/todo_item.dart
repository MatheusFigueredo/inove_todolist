import 'package:flutter/material.dart';
import 'controller.dart';

class ToDoItem extends StatefulWidget {
  final Map<String, dynamic> task;
  ToDoItem({required this.task});

  @override
  _ToDoItemState createState() => _ToDoItemState();
}

class _ToDoItemState extends State<ToDoItem> {
  @override
  Widget build(BuildContext context) {
    Controller controller = Controller();
    return Dismissible(
      key: Key(DateTime.now().millisecondsSinceEpoch.toString()),
      background: Container(
        color: Colors.red,
        child: Align(
          alignment: Alignment(-0.9, 0.0),
          child: Icon(
            Icons.delete,
            color: Colors.white,
          ),
        ),
      ),
      direction: DismissDirection.startToEnd,
      child: CheckboxListTile(
        value: widget.task['done'],
        title: Text(widget.task['title']),
        secondary: CircleAvatar(
          child: Icon(
            widget.task['done'] ? Icons.check : Icons.error,
          ),
        ),
        onChanged: (value) {
          setState(() {
            if (value != null) {
              widget.task['done'] = value;
              controller.saveTasks();
            }
          });
        },
      ),
      onDismissed: (_) {
        controller.remove(widget.task);
        controller.saveTasks();
      },
    );
  }
}
